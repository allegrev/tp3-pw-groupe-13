% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : **Utilisation de _The Movie DataBase_**

## Participants 

* Joan Besante
* Victor Allègre

## Mise en jambes

1. Ce qui nous est renvoyé par le serveur de TMDB est au format JSON, l'extension installée nous permet de l'analyser et de nous rendre compte qu'il s'agit de la fiche du film _Fight Club_ comme on peut le voir sous le label *original_title*. </br> En rajoutant le paramètre `language=fr` on peut voir que les informations demandées telles que le résumé du film sont envoyées en français.

2. Lorsqu'on effectue la même requête que précédemment mais avec `curl` on s'aperçoit que le service nous renvoie la même chose que sur le navigateur internet c'est-à-dire en format JSON. En utilisant la fonction fournie dans le fichier helper on obtient également le même résultat.

## Les choses sérieuses
   
8. Nous n'avons pas pu trouver de solutions afin d'afficher les acteurs qui jouent les hobbits car dans les crédits du film il n'est nullement indiqué si tel acteur joue un hobbit ou non mais seulement son rôle donc hormis filtrer leur nom un par un il nous paraît impossible de pouvoir différencier ces acteurs des autres.