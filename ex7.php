<html>
<body>
<?php
include "./tp3-helpers.php";

for ($i=1; $i <= 3; $i++) { 
    $lotr[$i] = json_decode(tmdbget('movie/12'.($i-1).'/credits'),true);    // get the json file of the LOTR trilogy
}	

for ($i=1; $i <= 3; $i++) { 
    $nbacteurs[$i] = sizeof($lotr[$i]["cast"]);     // create arrays to fill them of every actors of each movie
} 

$listeacteurs = array();

for ($j=1; $j <= 3; $j++) { 
    for($i=0;$i<$nbacteurs[$j];$i++){
        $acteurs[$j][$i] = $lotr[$j]["cast"][$i];		// fill an array to use it for the display
        array_push($listeacteurs, $lotr[$j]["cast"][$i]['name']);		// fill a list of all actors in the trilogy
    }
}

$listeacteurs = array_count_values($listeacteurs);		// that function allows to count the frequency for every key, so we know how many movie(s) each actor has played

for ($j=1; $j <= 3; $j++) {
	echo '<br><br><table>
	<CAPTION> LOTR '.$j.' </CAPTION>
	<tr>
	<th> Nom de l acteur </th>
	<th> Rôle </th>
	<th> Nombre d\'apparitions dans la trilogie </th>
	</tr>';
	for($i=0;$i<$nbacteurs[$j];$i++){		// display every actor of the first movie and how many movie he has played
		if($acteurs[$j][$i]["character"] != NULL){
			echo '
			<tr>
			<td><a href="https://www.themoviedb.org/person/'.$acteurs[$j][$i]["id"].'">'.$acteurs[$j][$i]["name"].'</a> </td>	
			<td>'.$acteurs[$j][$i]["character"].'</td>
			<td>'.$listeacteurs[$acteurs[$j][$i]["name"]].'</td>
			</tr>';		// link into TMDB actor page to see the list of films in which he participated
		}
	}
}

?>
</table>
</body>
</html>