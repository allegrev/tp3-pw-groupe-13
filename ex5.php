<html><body>
<?php 
include "./tp3-helpers.php";
$movieID = '550';       //Fight Club ID
$link = "movie/".$movieID;
$movies[0] = json_decode(tmdbget($link),true);  // get json file original version of movie
$movies[1] = json_decode(tmdbget($link, ['language'=>'en']),true);   // get json file english version of movie
$movies[2] = json_decode(tmdbget($link, ['language'=>'fr']),true);   // get json file french version of movie

for ($i=0; $i <= 2; $i++) { 
    $img[$i] = '<img src="https://image.tmdb.org/t/p/w200'.$movies[$i]['poster_path'].'">';
}


echo '<table>';
echo '<tr>';
for ($i=0; $i <= 2; $i++) { 
    echo '<td> 
            <table> 
                <tr> 
                    <td>Titre : '.$movies[$i]['title'].'</td>
                </tr>
                <tr>
                    <td>Titre original : '.$movies[$i]['original_title'].'</td>
                </tr>
                <tr>
                    <td>Tagline : '.$movies[$i]['tagline'].'</td>
                </tr>
                <tr>
                    <td>Description : '.$movies[$i]['overview'].'</td>
                </tr>
                <tr>
                    <td><a href="https://www.themoviedb.org/movie/'.$movieID.'">Lien TMDB</a></td>
                </tr>
                <tr>
                    <td>.'.$img[$i].'</td>
                </tr>
            </table>
        </td>';
}
echo '</tr>';
echo '</table>';

$link = "movie/".$movieID."/videos";
$movievideo = json_decode(tmdbget($link),true);     // get the json file of the video page of the movie

echo ' <iframe width="420" height="315"
src="https://www.youtube.com/embed/'.$movievideo['results'][0]['key'].'">       
</iframe> ';        // display embedded youtube video with the key recovered from the json file

?>
</body></html>
